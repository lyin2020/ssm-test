import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Demo {
    /*
    如何删除list中重复项，并不影响排序？
     */
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(5);
        list.add(7);
        list.add(15);

        System.out.println(list.toString());

        //放入Set集合中
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        for (Integer i:list) {
            hashSet.add(i);
        }

        System.out.println(hashSet);


    }
}
