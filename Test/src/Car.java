public class Car extends Vehicles {

    public Car(String brand, String color) {
        super(brand, color);
    }

    public Car(String brand, String color, int seats) {
        super(brand, color);
        this.seats = seats;
    }

    private int seats;

    public void showCar(){
        System.out.println("商标："+getBrand()+",颜色:"+getColor()+",座位数："+seats);
    }
}
