public class Test01 {
    public static void main(String[] args) {
        int arr[] ={1,3,6,7,9};//有序数组

        System.out.println(binarySearch(arr,6));

    }
    public static int binarySearch(int arr[],int a){
        int begin=0;
        int end =arr.length-1;
        while(begin<=end){
            int mid=(begin+end)/2;
            if (a>arr[mid]){
                begin=mid+1;
            }else if (a<arr[mid]){
                end=mid-1;
            }else {
                return mid;
            }
        }
        return -1;
    }
}
